"use strict";

function plot(ys, yTitle, filename) {
    // initialisation
    const fs = require('fs');
    const JSDOM = require('jsdom').JSDOM;
    const jsdom = new JSDOM('<body><div id="container"></div></body>', {runScripts: 'dangerously'});
    const window = jsdom.window;
    const anychart = require('anychart')(window);
    const anychartExport = require('anychart-nodejs')(anychart);

    // dessin
    const chart = anychart.line();
    const series = chart.line(ys);
    series.stroke('2 blue');
    chart.bounds(0, 0, 640, 480);

    const xAxis = chart.xAxis();
    xAxis.labels().fontColor("black").fontWeight(1000);
    xAxis.title('x').stroke('1 black');
    xAxis.title().fontColor("black").fontSize(20);
    chart.xGrid().enabled(true).stroke("1 grey");

    const yAxis = chart.yAxis();
    yAxis.labels().fontColor("black").fontWeight(1000);
    yAxis.title(yTitle).stroke('1 black');
    yAxis.title().fontColor("black").fontSize(20);
    chart.yGrid().enabled(true).stroke("1 grey");

    chart.container('container');
    chart.draw();

    // export
    anychartExport.exportTo(chart, 'png').then(function(image) {
        fs.writeFile(filename, image, function(fsWriteError) {
            if (fsWriteError) {
                console.log(fsWriteError);
            } else {
                console.log('Complete');
            }
        });
    }, function(generationError) {
        console.log(generationError);
    });
}

const fibo = require("./fibo");
const ysFibo = Array.from({length: 10}, (_,i) => [i, fibo.fiboIterative(i)]);
plot(ysFibo, 'fiboIterative(x)', 'plot_fibo.png');

